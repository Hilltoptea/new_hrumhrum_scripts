## Скрипт деплоя проектов на новой машине для hrumhrum.me
Пример выполнения скрипта:
```bash
python3 deploy.py web-shop-js preproduction git@bitbucket.org:4-mobile 4d1cff5f9fc36fd2beec63f7d44ab0054d71118a
```

Также доступна помощь по командам:
```bash
python3 deploy.py -h
```
