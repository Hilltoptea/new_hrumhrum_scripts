# -*- coding: utf-8 -*-

import argparser as arg
from datetime import datetime
import os
import random
import sys

def execute(statement):
    #for executing system commands
    print('Executing "%s"' %(statement))
    if os.system(statement) != 0:
        print('Cannot exec statement: %s' %(statement))
        sys.exit()

def get_dir_name(sha):
    return '%s_%s' %(datetime.now().strftime('%d-%m-%y_%H-%M-%S'),sha[:6])

def make_app_dir(prj_dir,app,env):
    os.makedirs(os.path.join(work_dir,env,app,prj_dir))

def clone(repository, app, sha):
    execute('git clone %s/%s.git .' %(repository, app))
    execute('git reset --hard %s' %(sha))
    execute('git pull')

def clean_dirs(app,env,revision_count):
    dirlist = os.listdir(os.path.join(work_dir,env, app))
    os.chdir(os.path.join(work_dir,env, app))
    print('Cleaning dirs...')
    while len(dirlist) > revision_count:
        directory = min(dirlist, key=os.path.getctime)
        execute('rm -r %s' %(os.path.join(work_dir,env, app, directory)))
        dirlist.remove(directory)

def proj_is_running():
    return False

def dir_exist(prj_dir):
    return os.path.exists(prj_dir)

if __name__ == '__main__':
    global work_dir
    work_dir = '/opt/'
    prj_dir = get_dir_name(arg.sha)
    max_revision_count = 10

    os.chdir(os.path.join(work_dir,arg.env, arg.app))
    if not dir_exist(prj_dir):
        make_app_dir(prj_dir,arg.app,arg.env)
        os.chdir(prj_dir)
        clone(arg.repository, arg.app, arg.sha)
    else:
        os.chdir(prj_dir)
    os.chdir('Docker')
    execute('sudo bash build.sh %s %s %s' %(arg.app, arg.sha, arg.env))
    clean_dirs(arg.app,arg.env,max_revision_count)
