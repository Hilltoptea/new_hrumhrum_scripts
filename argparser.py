# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("application", help="Name of application. Use: shop-api or web-shop-js or sitemap-generator")
parser.add_argument("enviroment", help="Name of enviroment. Use: qa or production")
parser.add_argument("repository", help="Name of repository.")
parser.add_argument("SHA", help="Commit hash")
#parser.add_argument("-dc","--deleteCache", action="store_true", help="Use -dc for deleting cache")
args = parser.parse_args()

app = args.application
env = args.enviroment
repository = args.repository
sha = args.SHA
